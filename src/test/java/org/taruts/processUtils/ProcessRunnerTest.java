package org.taruts.processUtils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.SystemUtils;
import org.junit.jupiter.api.Test;

import java.io.File;

@Slf4j
class ProcessRunnerTest {

    @Test
    void runProcess() {
        File directory = new File(".");
        if (SystemUtils.IS_OS_WINDOWS) {
            ProcessRunner.runProcess(directory, "cmd.exe", "/c", "echo", "hello");
        } else {
            ProcessRunner.runProcess(directory, "echo", "hello");
        }
    }
}
