package org.taruts.processUtils;

public class ExitStatusException extends RuntimeException {

    private final int status;

    public ExitStatusException(String commandName, int status) {
        super("" + commandName + " exited with status " + status);
        this.status = status;
    }

    public int getStatus() {
        return status;
    }
}
