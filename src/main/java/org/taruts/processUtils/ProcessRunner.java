package org.taruts.processUtils;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Stream;

@Slf4j
public class ProcessRunner {

    @SneakyThrows
    public static String runProcess(File directory, boolean inheritIO, String... command) {

        command = Arrays
                .stream(command)
                .filter(StringUtils::isNotBlank)
                .toArray(String[]::new);

        ProcessBuilder processBuilder = new ProcessBuilder(command);
        processBuilder.directory(directory);

        if (inheritIO) {
            processBuilder.inheritIO();
        }

        Process process = processBuilder.start();

        try (
                BufferedReader errorReader = process.errorReader();
                BufferedReader inputReader = process.inputReader();
        ) {
            List<String> stdoutLines = new ArrayList<>();

            processReaderInThread(
                    inputReader,
                    stdoutLine -> {
                        log.info(stdoutLine);
                        stdoutLines.add(stdoutLine);
                    },
                    e -> log.error("Error while reading stdout", e)
            );

            processReaderInThread(
                    errorReader,
                    log::error,
                    e -> log.error("Error while reading stderr", e)
            );

            int exitStatus = process.waitFor();

            if (exitStatus == 0) {
                return String.join("\n", stdoutLines);
            } else {
                throw new ExitStatusException(command[0], exitStatus);
            }
        }
    }

    public static String runProcess(File directory, boolean inheritIO, List<String> command) {
        return runProcess(directory, inheritIO, commandListToArray(command));
    }

    public static String runProcess(File directory, String... command) {
        return runProcess(directory, false, command);
    }

    public static String runProcess(File directory, List<String> command) {
        return runProcess(directory, false, commandListToArray(command));
    }

    public static String runScript(File directory, boolean inheritIO, String scriptFilename, String[] parameters) {
        List<String> commandsList = new ArrayList<>();
        if (!SystemUtils.IS_OS_WINDOWS) {
            commandsList.add("sh");
        }

        String filePath;
        try {
            filePath = directory.getCanonicalPath();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        String pathStr = Path.of(filePath, scriptFilename).toString();
        commandsList.add(pathStr);

        commandsList.addAll(Arrays.asList(parameters));

        return runProcess(directory, inheritIO, commandsList);
    }

    public static String runScript(File directory, boolean inheritIO, String scriptFilename, List<String> parameters) {
        return runScript(directory, inheritIO, scriptFilename, commandListToArray(parameters));
    }

    public static String runScript(File directory, String scriptFilename, String... parameters) {
        return runScript(directory, false, scriptFilename, parameters);
    }

    public static String runScript(File directory, String scriptFilename, List<String> parameters) {
        return runScript(directory, false, scriptFilename, commandListToArray(parameters));
    }

    public static String runScript(
            File directory,
            boolean inheritIO,
            String scriptFilename,
            String windowsExtension,
            String nixExtension,
            String... parameters
    ) {
        String extension = SystemUtils.IS_OS_WINDOWS ? windowsExtension : nixExtension;
        String filename = getFilenameWithExtension(scriptFilename, extension);
        return runScript(directory, inheritIO, filename, parameters);
    }

    public static String runScript(
            File directory,
            boolean inheritIO,
            String scriptFilename,
            String windowsExtension,
            String nixExtension,
            List<String> parameters
    ) {
        return runScript(
                directory,
                inheritIO,
                scriptFilename,
                windowsExtension,
                nixExtension,
                commandListToArray(parameters)
        );
    }

    public static String runScript(
            File directory,
            String scriptFilename,
            String windowsExtension,
            String nixExtension,
            String... parameters
    ) {
        return runScript(
                directory,
                false,
                scriptFilename,
                windowsExtension,
                nixExtension,
                parameters
        );
    }

    public static String runScript(
            File directory,
            String scriptFilename,
            String windowsExtension,
            String nixExtension,
            List<String> parameters
    ) {
        return runScript(
                directory,
                false,
                scriptFilename,
                windowsExtension,
                nixExtension,
                commandListToArray(parameters)
        );
    }

    public static String runBatOrSh(File directory, boolean inheritIO, String scriptFilename, String... parameters) {
        return runScript(directory, inheritIO, scriptFilename, "bat", "sh", parameters);
    }

    public static String runBatOrSh(File directory, boolean inheritIO, String scriptFilename, List<String> parameters) {
        return runScript(directory, inheritIO, scriptFilename, "bat", "sh", commandListToArray(parameters));
    }

    public static String runBatOrSh(File directory, String scriptFilename, String... parameters) {
        return runScript(directory, false, scriptFilename, "bat", "sh", parameters);
    }

    public static String runBatOrSh(File directory, String scriptFilename, List<String> parameters) {
        return runScript(directory, false, scriptFilename, "bat", "sh", commandListToArray(parameters));
    }

    public static String runCmdOrSh(File directory, boolean inheritIO, String scriptFilename, String... parameters) {
        return runScript(directory, inheritIO, scriptFilename, "cmd", "sh", parameters);
    }

    public static String runCmdOrSh(File directory, boolean inheritIO, String scriptFilename, List<String> parameters) {
        return runScript(directory, inheritIO, scriptFilename, "cmd", "sh", commandListToArray(parameters));
    }

    public static String runCmdOrSh(File directory, String scriptFilename, String... parameters) {
        return runScript(directory, false, scriptFilename, "cmd", "sh", parameters);
    }

    public static String runCmdOrSh(File directory, String scriptFilename, List<String> parameters) {
        return runScript(directory, false, scriptFilename, "cmd", "sh", commandListToArray(parameters));
    }

    public static String runBat(File directory, boolean inheritIO, String scriptFilename, String... parameters) {
        return runScript(directory, inheritIO, scriptFilename, "bat", null, parameters);
    }

    public static String runBat(File directory, boolean inheritIO, String scriptFilename, List<String> parameters) {
        return runScript(directory, inheritIO, scriptFilename, "bat", null, commandListToArray(parameters));
    }

    public static String runBat(File directory, String scriptFilename, String... parameters) {
        return runScript(directory, false, scriptFilename, "bat", null, parameters);
    }

    public static String runBat(File directory, String scriptFilename, List<String> parameters) {
        return runScript(directory, false, scriptFilename, "bat", null, commandListToArray(parameters));
    }

    public static String runCmd(File directory, boolean inheritIO, String scriptFilename, String... parameters) {
        return runScript(directory, inheritIO, scriptFilename, "cmd", null, parameters);
    }

    public static String runCmd(File directory, boolean inheritIO, String scriptFilename, List<String> parameters) {
        return runScript(directory, inheritIO, scriptFilename, "cmd", null, commandListToArray(parameters));
    }

    public static String runCmd(File directory, String scriptFilename, String... parameters) {
        return runScript(directory, false, scriptFilename, "cmd", null, parameters);
    }

    public static String runCmd(File directory, String scriptFilename, List<String> parameters) {
        return runScript(directory, false, scriptFilename, "cmd", null, commandListToArray(parameters));
    }

    public static String runExecutable(File directory, boolean inheritIO, String... command) {
        List<String> commandsList = new ArrayList<>();
        if (SystemUtils.IS_OS_WINDOWS) {
            commandsList.add("cmd");
            commandsList.add("/c");
        } else {
            commandsList.add("sh");
            commandsList.add("-c");
        }

        String commandStr = String.join(" ", command);
        commandStr = '"' + commandStr + '"';
        commandsList.add(commandStr);

        return runProcess(directory, inheritIO, commandsList);
    }

    public static String runExecutable(
            File directory,
            boolean inheritIO,
            String executableName,
            List<String> parameters
    ) {
        return runExecutable(directory, inheritIO, commandListToArray(executableName, parameters));
    }

    public static String runExecutable(File directory, boolean inheritIO, List<String> command) {
        return runExecutable(directory, inheritIO, commandListToArray(command));
    }

    public static String runExecutable(File directory, String... command) {
        return runExecutable(directory, false, command);
    }

    public static String runExecutable(File directory, String executableName, List<String> parameters) {
        return runExecutable(directory, false, commandListToArray(executableName, parameters));
    }

    public static String runExecutable(File directory, List<String> command) {
        return runExecutable(directory, false, commandListToArray(command));
    }

    public static int getStatus(Runnable runnable) {
        try {
            runnable.run();
            return 0;
        } catch (ExitStatusException e) {
            return e.getStatus();
        }
    }

    public static boolean isSuccess(Runnable runnable) {
        try {
            runnable.run();
            return true;
        } catch (ExitStatusException e) {
            return false;
        }
    }

    private static String[] commandListToArray(List<String> commandList) {
        return commandList.toArray(String[]::new);
    }

    private static String[] commandListToArray(String executableName, List<String> parameters) {
        return Stream.concat(
                Stream.of(executableName),
                parameters.stream()
        ).toArray(String[]::new);
    }

    private static String getFilenameWithExtension(String filename, String extension) {
        if (StringUtils.isBlank(extension)) {
            return filename;
        } else {
            return filename + "." + extension;
        }
    }

    private static void processReaderInThread(
            BufferedReader bufferedReader,
            Consumer<String> lineConsumer,
            Consumer<IOException> exceptionConsumer
    ) {
        new Thread(() -> {
            while (true) {
                String line;
                try {
                    line = bufferedReader.readLine();
                } catch (IOException e) {
                    exceptionConsumer.accept(e);
                    return;
                }
                if (line == null) {
                    return;
                }
                if (StringUtils.isNotBlank(line)) {
                    lineConsumer.accept(line);
                }
            }
        }).start();
    }
}
