package org.taruts.processUtils;

import org.apache.commons.lang3.SystemUtils;

import java.io.File;

public class Main {
    public static void main(String[] args) {
        File directory = new File(".");
        String output;
        if (SystemUtils.IS_OS_WINDOWS) {
            output = ProcessRunner.runProcess(directory, "cmd.exe", "/c", "echo", "hello");
        } else {
            output = ProcessRunner.runProcess(directory, "echo", "hello");
        }
        System.out.println("output: " + output);
    }
}
